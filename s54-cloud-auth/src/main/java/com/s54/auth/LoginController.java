package com.s54.auth;

import ch.qos.logback.core.subst.Token;
import com.s54.common.beans.LoginVO;
import com.s54.common.beans.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/user")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public R<Token> login(@RequestBody LoginVO loginVO) {
        log.debug("login:{}:", loginVO);
        return R.ok(loginService.login(loginVO));
    }

    @GetMapping("/logout")
    public R logout(@RequestHeader("Access-Token") String accessToken) {
        return loginService.logout(accessToken) ? R.ok() : R.err("注销失败");
    }

}
