package com.s54.api;

import com.s54.common.context.ThreadContextHolder;
import com.s54.common.enums.SecurityEnum;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class FeignRequestInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        String token = ThreadContextHolder.getHttpRequest().getHeader(SecurityEnum.HEADER_TOKEN.getValue());
        if (StringUtils.hasText(token)) {
            requestTemplate.header(SecurityEnum.HEADER_TOKEN.getValue(), token);
        }
    }
}
