package com.s54.api.system;


import com.s54.common.beans.R;
import com.s54.common.beans.UserInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Set;

@FeignClient(name = "s54-system", path = "/system/users", contextId = "UserApi")
public interface UserApi {
    @GetMapping("/getUserInfo")
    public R<UserInfo> getUserInfo(@RequestParam("username") String username);

    @GetMapping("/getApiPaths")
    public R<Set<String>> getApiPaths(@RequestParam("username") String username);
}
