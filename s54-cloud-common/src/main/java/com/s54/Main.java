package com.s54;

import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        System.out.println("DigestUtils.md5DigestAsHex(\"你好\".getBytes(StandardCharsets.UTF_8)) = " + DigestUtils.md5DigestAsHex("你好".getBytes(StandardCharsets.UTF_8)));
        System.out.println("DigestUtils.md5DigestAsHex(\"你好\".getBytes(StandardCharsets.UTF_8)) = " + DigestUtils.md5DigestAsHex("你好".getBytes(StandardCharsets.UTF_8)));
        System.out.println("DigestUtils.md5DigestAsHex(\"123\".getBytes(StandardCharsets.UTF_8)) = " + DigestUtils.md5DigestAsHex("123".getBytes(StandardCharsets.UTF_8)));

    }
}