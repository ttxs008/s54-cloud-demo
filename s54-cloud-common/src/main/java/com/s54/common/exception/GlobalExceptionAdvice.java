package com.s54.common.exception;

import com.s54.common.beans.R;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionAdvice {
    @ExceptionHandler(LoginException.class)
    public R handler(LoginException ex) {
        return R.err("登录失败" + ex.getMessage());
    }

    @ExceptionHandler(ServiceException.class)
    public R handler(ServiceException ex) {
        return R.err("业务失败：" + ex.getMessage());
    }


    @ExceptionHandler(Exception.class)
    public R handler(Exception ex) {
        return R.err("系统错误：" + ex.getMessage());
    }
}
