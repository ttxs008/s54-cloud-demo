package com.s54.common.enums;

public enum GlobalEnum {
    STATUS_DISABLED("停用", "1"),
    STATUS_ENABLED("启用", "0");

    GlobalEnum(String label, String value) {
        this.lable = label;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getLable() {
        return lable;
    }

    private String lable;
    private String value;
}
