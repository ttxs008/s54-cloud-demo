package com.s54.common.enums;

import com.s54.common.constants.CacheConstants;

public enum CachePrefix {
    /**
     * PC端token前缀
     */
    PC_TOKEN,
    /**
     * 移动端token前缀
     */
    MOBILE_TOKEN;

    public String getPrefix() {
        return String.join(CacheConstants.DELIMITER, CacheConstants.PREFIX, this.name());
    }

}
