package com.s54.common.enums;

public enum SecurityEnum {
    HEADER_TOKEN("Access-Token");
    SecurityEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    private String value;
}
