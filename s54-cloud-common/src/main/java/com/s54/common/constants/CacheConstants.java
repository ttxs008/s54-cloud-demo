package com.s54.common.constants;

public interface CacheConstants {
    String PREFIX = "S54";
    String DELIMITER = ":";
    /**
     * 过期时间，分钟
     */
    int EXPIRE_TIME = 30;
}
