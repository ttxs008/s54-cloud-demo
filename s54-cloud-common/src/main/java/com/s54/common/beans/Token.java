package com.s54.common.beans;

import lombok.Data;

@Data
public class Token {
    private String token;

    public Token() {}
    public Token(String token) {
        this.token = token;
    }
}
