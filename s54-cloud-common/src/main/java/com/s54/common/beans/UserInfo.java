package com.s54.common.beans;

import lombok.Data;

@Data
public class UserInfo {
    private Long userId;
    private Long deptId;
    private String userName;
    private String nickName;
    private String userType;
    private String email;
    private String phonenumber;
    private String sex;
    private String avatar;
    private String password;
    private String status;
}
