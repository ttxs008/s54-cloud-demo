package com.s54.common.beans;

import lombok.Data;

@Data
public class LoginVO {
    private String username;
    private String password;
}
