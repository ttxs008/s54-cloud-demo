package com.s54.common.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class R<T> {
    public static final int SUCCESS = 200;
    private int code;
    private String message;
    private T data;

    public R() {}
    private R(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    @JsonIgnore
    public boolean isFailed() {
        return code != SUCCESS;
    }

    public static R ok() {
        return new R(SUCCESS, "成功", null);
    }

    public static <T> R ok(T data) {
        return new R(SUCCESS, "成功", data);
    }

    public static R err(String message) {
        return new R(500, message, null);
    }

    public static R err(int code, String message) {
        return new R(code, message, null);
    }
}
