package com.s54.common.beans;

import lombok.Data;

import java.util.Set;

/**
 * 授权用户，登录成功后，保存到缓存
 */
@Data
public class AuthUser {
    private String username;
    private String nickName;
    private String sex;
    private Set<String> apiPaths;
}
