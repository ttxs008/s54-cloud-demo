package com.s54.system.service;

import com.s54.system.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户和角色关联表 服务类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface IUserRoleService extends IService<UserRole> {

}
