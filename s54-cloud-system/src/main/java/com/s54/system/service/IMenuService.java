package com.s54.system.service;

import com.s54.system.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface IMenuService extends IService<Menu> {

    List<Menu> getByUsername(String username);
}
