package com.s54.system.service;

import com.s54.system.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Set;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface IUserService extends IService<User> {

    Set<String> getApiPaths(String username);
}
