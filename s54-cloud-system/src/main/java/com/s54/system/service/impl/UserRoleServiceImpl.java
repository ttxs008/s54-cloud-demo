package com.s54.system.service.impl;

import com.s54.system.entity.UserRole;
import com.s54.system.mapper.UserRoleMapper;
import com.s54.system.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户和角色关联表 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
