package com.s54.system.service.impl;

import com.s54.system.entity.Menu;
import com.s54.system.mapper.MenuMapper;
import com.s54.system.service.IMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Override
    public List<Menu> getByUsername(String username) {
        return getBaseMapper().selectByUsername(username);
    }
}
