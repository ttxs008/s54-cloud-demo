package com.s54.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.s54.system.entity.Menu;
import com.s54.system.entity.User;
import com.s54.system.mapper.UserMapper;
import com.s54.system.service.IMenuService;
import com.s54.system.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    private final IMenuService menuService;
    @Override
    public Set<String> getApiPaths(String username) {
        List<Menu> menus = menuService.getByUsername(username);
        return menus.stream().filter(item -> StringUtils.hasText(item.getApiPath()))
                .map(item -> item.getHttpMethod().toUpperCase() + "_" + item.getApiPath())
                .collect(Collectors.toSet());
    }
}
