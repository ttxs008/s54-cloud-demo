package com.s54.system.service;

import com.s54.system.entity.RoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface IRoleMenuService extends IService<RoleMenu> {

}
