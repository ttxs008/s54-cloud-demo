package com.s54.system.service;

import com.s54.system.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface IRoleService extends IService<Role> {

}
