package com.s54.system.service.impl;

import com.s54.system.entity.Role;
import com.s54.system.mapper.RoleMapper;
import com.s54.system.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色信息表 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
