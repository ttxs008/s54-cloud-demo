package com.s54.system.service.impl;

import com.s54.system.entity.RoleMenu;
import com.s54.system.mapper.RoleMenuMapper;
import com.s54.system.service.IRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色和菜单关联表 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {

}
