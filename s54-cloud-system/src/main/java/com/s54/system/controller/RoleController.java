package com.s54.system.controller;

import com.s54.common.beans.R;
import com.s54.system.entity.Role;
import com.s54.system.service.IRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 角色信息表 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
@RestController
@RequestMapping("/system/roles")
@RequiredArgsConstructor
public class RoleController {
    private final IRoleService roleService;

    @GetMapping
    public R getAll() {
        return R.ok(roleService.list());
    }

    @PostMapping
    public R add(@RequestBody Role role) {
        roleService.save(role);
        return R.ok();
    }

    @DeleteMapping("/{id}")
    public R del(@PathVariable Long id) {
        roleService.removeById(id);
        return R.ok();
    }

    @PutMapping
    public R update(@RequestBody Role role) {
        roleService.saveOrUpdate(role);
        return R.ok();
    }
}
