package com.s54.system.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 角色和菜单关联表 前端控制器
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
@Controller
@RequestMapping("/system/roleMenu")
public class RoleMenuController {

}
