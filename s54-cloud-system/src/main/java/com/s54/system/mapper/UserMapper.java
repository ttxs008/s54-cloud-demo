package com.s54.system.mapper;

import com.s54.system.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface UserMapper extends BaseMapper<User> {

}
