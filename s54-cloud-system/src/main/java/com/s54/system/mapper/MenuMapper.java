package com.s54.system.mapper;

import com.s54.system.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> selectByUsername(String username);
}
