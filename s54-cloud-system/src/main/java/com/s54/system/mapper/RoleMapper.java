package com.s54.system.mapper;

import com.s54.system.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色信息表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface RoleMapper extends BaseMapper<Role> {

}
