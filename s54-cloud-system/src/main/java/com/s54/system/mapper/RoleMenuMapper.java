package com.s54.system.mapper;

import com.s54.system.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}
