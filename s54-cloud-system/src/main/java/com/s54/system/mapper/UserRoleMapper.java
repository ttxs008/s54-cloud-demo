package com.s54.system.mapper;

import com.s54.system.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户和角色关联表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2023-05-16
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
